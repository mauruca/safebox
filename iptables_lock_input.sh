#!/bin/sh

iptables -A INPUT -p tcp -syn -j DROP
iptables -A INPUT -m state —state ESTABLISHED,RELATED -j ACCEPT

iptables -N LOGDROP
ptables -A logdrop -J LOG
iptables -A logdrop -J DROP
